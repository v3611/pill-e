# Pill-e

A feature-rich but simple to use pill cap medication reminder.

Initial design mockup (I'd appreciate input):
![mockup](pill-e_mockup_topview.png)

# Usage

This device is designed to be reusable, as well as easy to use.

Before configuration, you should note which meds you're setting the reminder for in the first place. 
This can be done with a "permanent" marker, which typically can be wiped off very easily with some isopropyl alcohol.

All that's really necessary is setting the device clock and dosage times. By default, all dosage times are set to OFF.
Once those are set, the device will keep track of the appropriate times for the next dose, so you won't have to worry about pressing the wrong button when dismissing a notification. 

Missing a dose (if you ignore all the sounds and blinking for up to 2 hours) will time out and end up just skipped. In order to prevent this, a version featuring a low power wireless radio is also planned. It could notify you or a caretaker through other channels.
